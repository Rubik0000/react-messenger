package datalayer.entities;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "dialogs")
public class Dialog {

    @Id @GeneratedValue
    @Column(name = "id_dialog")
    private long idDialog;

    @Column(name = "name")
    private String dialogName;

    @Column(name = "deleted")
    private boolean deleted;

    @ManyToMany(mappedBy = "dialogs")
    private List<User> users;

    @OneToMany(mappedBy = "dialog")
    private List<Message> messages;

    public Dialog() { }

    public Dialog(String name) {
        dialogName = name;
    }

    public long getIdDialog() {
        return idDialog;
    }

    public void setIdDialog(long idDialog) {
        this.idDialog = idDialog;
    }

    public String getDialogName() {
        return dialogName;
    }

    public void setDialogName(String dialogName) {
        this.dialogName = dialogName;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    public List<Message> getMessages() {
        return messages;
    }

    public void setMessages(List<Message> messages) {
        this.messages = messages;
    }
}
