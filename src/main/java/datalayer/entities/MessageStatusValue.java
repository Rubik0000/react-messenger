package datalayer.entities;

public enum  MessageStatusValue {
    sent,
    edited,
    deleted,
}
