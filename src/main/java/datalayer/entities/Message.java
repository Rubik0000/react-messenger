package datalayer.entities;

import javax.persistence.*;
import java.util.Calendar;

@Entity
@Table(name = "messages")
public class Message {
    @Id @GeneratedValue
    @Column(name = "id_message")
    private long idMessage;

    @ManyToOne
    @JoinColumn(name = "id_user")
    private User user;

    @ManyToOne
    @JoinColumn(name = "id_dialog")
    private Dialog dialog;

    @ManyToOne
    @JoinColumn(name = "id_status")
    private MessageStatus status;

    @Column(name = "text")
    private String text;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "date")
    private Calendar date;

    public Message() { }

    public Message(String text, Calendar date) {
        this.text = text;
        this.date = date;
    }

    public long getIdMessage() {
        return idMessage;
    }

    public void setIdMessage(long idMessage) {
        this.idMessage = idMessage;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Dialog getDialog() {
        return dialog;
    }

    public void setDialog(Dialog dialog) {
        this.dialog = dialog;
    }

    public MessageStatus getStatus() {
        return status;
    }

    public void setStatus(MessageStatus status) {
        this.status = status;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Calendar getDate() {
        return date;
    }

    public void setDate(Calendar date) {
        this.date = date;
    }
}
