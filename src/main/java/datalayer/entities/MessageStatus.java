package datalayer.entities;

import javax.persistence.*;

@Entity
@Table(name = "message_status")
public class MessageStatus {

    @Id @GeneratedValue
    @Column(name = "id_status")
    private int idStatus;

    @Enumerated(EnumType.STRING)
    @Column(name = "status", nullable = false, unique = true)
    private MessageStatusValue status;

    public MessageStatus() { }

    public MessageStatus(MessageStatusValue statusValue) {
        status = statusValue;
    }

    public int getIdStatus() {
        return idStatus;
    }

    public void setIdStatus(int idStatus) {
        this.idStatus = idStatus;
    }

    public MessageStatusValue getStatus() {
        return status;
    }

    public void setStatus(MessageStatusValue status) {
        this.status = status;
    }
}
