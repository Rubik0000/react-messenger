package datalayer.sessions;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class SessionManager {
    private static SessionManager sessionManager;

    private SessionFactory factory;

    private SessionManager() {
        factory = new Configuration()
                .addAnnotatedClass(datalayer.entities.User.class)
                .addAnnotatedClass(datalayer.entities.MessageStatus.class)
                .addAnnotatedClass(datalayer.entities.Dialog.class)
                .addAnnotatedClass(datalayer.entities.Message.class)
                .configure()
                .buildSessionFactory();
    }


    public static SessionManager getInstance() {
        if (sessionManager == null) {
            sessionManager = new SessionManager();
        }
        return sessionManager;
    }

    public Session openSession() {
        return factory.openSession();
    }

}
