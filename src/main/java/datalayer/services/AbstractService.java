package datalayer.services;

import datalayer.entities.Message;
import datalayer.entities.User;
import datalayer.services.exceptions.TransactionErrorException;
import datalayer.sessions.SessionManager;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.TransactionException;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.io.Serializable;
import java.util.List;

abstract class AbstractService {
    private Session session;

    protected final <T> T getById(Class<T> tClass, String fieldName, long id) {
        CriteriaBuilder criteriaBuilder = getCriteriaBuilder();
        CriteriaQuery<T> criteriaQuery = criteriaBuilder.createQuery(tClass);
        Root<T> root = criteriaQuery.from(tClass);
        criteriaQuery.select(root).where(criteriaBuilder.equal(root.get(fieldName), id));
        List<T> list = executeQuery(criteriaQuery);
        if (list.size() == 0) {
            return null;
        }
        return list.get(0);
    }

    protected final CriteriaBuilder getCriteriaBuilder() {
        if (session == null || !session.isOpen()) {
            session = SessionManager.getInstance().openSession();
        }
        return session.getCriteriaBuilder();
    }

    protected final <T> List<T> executeQuery(CriteriaQuery<T> criteriaQuery) {
        if (session == null || !session.isOpen()) {
            session = SessionManager.getInstance().openSession();
        }
        List<T> resultList = session.createQuery(criteriaQuery).getResultList();
        session.close();
        return resultList;
    }

    private Serializable doTransaction(Object object, SessionActions sessionAction) throws TransactionErrorException {
        Session session = SessionManager.getInstance().openSession();
        Transaction transaction = null;
        Serializable result = null;
        try {
            transaction = session.beginTransaction();
            switch (sessionAction) {
                case save:
                    result = session.save(object);
                    break;

                case delete:
                    session.delete(object);
                    break;

                default:
            }
            transaction.commit();
            return result;
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
            throw new TransactionErrorException(e.getMessage());
        } finally {
            session.close();
        }
    }

    protected final void deleteObject(Object object) throws TransactionErrorException {
        doTransaction(object, SessionActions.delete);
    }

    protected final Serializable saveObject(Object object) throws TransactionErrorException {
        return doTransaction(object, SessionActions.save);
    }
}
