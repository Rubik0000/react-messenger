package datalayer.services;

import datalayer.entities.MessageStatus;
import datalayer.entities.MessageStatusValue;
import datalayer.services.exceptions.TransactionErrorException;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

public class MessageStatusService extends AbstractService {
    static private boolean filled = false;

    public MessageStatusService() {
        try {
            if (!filled) {
                for (MessageStatusValue statusValue : MessageStatusValue.values()) {
                    if (findByStatus(statusValue) == null) {
                        saveObject(new MessageStatus(statusValue));
                    }
                }
                filled = true;
            }
        } catch (TransactionErrorException e) {
            e.printStackTrace();
        }
    }

    public List<MessageStatus> getAll() {
        CriteriaBuilder criteriaBuilder = getCriteriaBuilder();
        CriteriaQuery<MessageStatus> criteriaQuery = criteriaBuilder.createQuery(MessageStatus.class);
        Root<MessageStatus> root = criteriaQuery.from(MessageStatus.class);
        criteriaQuery.select(root);
        List<MessageStatus> list = executeQuery(criteriaQuery);
        return list;
    }

    public MessageStatus findByStatus(MessageStatusValue statusValue) {
        CriteriaBuilder criteriaBuilder = getCriteriaBuilder();
        CriteriaQuery<MessageStatus> criteriaQuery = criteriaBuilder.createQuery(MessageStatus.class);
        Root<MessageStatus> root = criteriaQuery.from(MessageStatus.class);
        criteriaQuery.select(root).where(criteriaBuilder.equal(root.get("status"), statusValue));
        List<MessageStatus> list = executeQuery(criteriaQuery);
        if (list.size() == 0) {
            return null;
        }
        return list.get(0);
    }
}
