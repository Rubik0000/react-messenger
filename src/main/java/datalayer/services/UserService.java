package datalayer.services;

import datalayer.entities.User;
import datalayer.services.exceptions.NoSuchRecordException;
import datalayer.services.exceptions.RecordExistsException;
import datalayer.services.exceptions.TransactionErrorException;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

public class UserService extends AbstractService {

    public User getUserById(long id) {
        return getById(User.class, "idUser", id);
    }

    public User getUserByNickname(String nickname) {
        CriteriaBuilder criteriaBuilder = getCriteriaBuilder();
        CriteriaQuery<User> criteriaQuery = criteriaBuilder.createQuery(User.class);
        Root<User> root = criteriaQuery.from(User.class);
        criteriaQuery.select(root).where(criteriaBuilder.equal(root.get("nickName"), nickname));
        List<User> list = executeQuery(criteriaQuery);
        if (list.size() == 0) {
            return null;
        }
        return list.get(0);
    }

    public long addUser(User user) throws TransactionErrorException, RecordExistsException {
        if (getUserByNickname(user.getNickName()) != null) {
            throw new RecordExistsException("The user with " + user.getNickName() + " nickname exists");
        }
        return (Long) saveObject(user);
    }

    public void deleteUser(User user) throws TransactionErrorException {
        deleteObject(user);
    }

    public void updateUser(User user) throws NoSuchRecordException, TransactionErrorException {
        if (getUserById(user.getIdUser()) == null) {
            throw new NoSuchRecordException("The user with " + user.getIdUser() + " id was not found");
        }
        saveObject(user);
    }
}
