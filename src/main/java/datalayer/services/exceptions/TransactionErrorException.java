package datalayer.services.exceptions;

public class TransactionErrorException extends Exception {
    public TransactionErrorException(String message) {
        super(message);
    }

    public TransactionErrorException() { }
}
