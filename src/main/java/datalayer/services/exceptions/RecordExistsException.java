package datalayer.services.exceptions;

public class RecordExistsException extends Exception {
    public RecordExistsException(String message) {
        super(message);
    }
}
