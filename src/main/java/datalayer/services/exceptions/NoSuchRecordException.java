package datalayer.services.exceptions;

public class NoSuchRecordException extends Exception {
    public NoSuchRecordException(String message) {
        super(message);
    }
}
