package datalayer.services;

import datalayer.entities.Message;
import datalayer.entities.MessageStatusValue;
import datalayer.services.exceptions.NoSuchRecordException;
import datalayer.services.exceptions.TransactionErrorException;

public class MessageService extends AbstractService {
    private MessageStatusService statusService = new MessageStatusService();

    public Message getMessageById(long id) {
        return getById(Message.class, "idMessage", id);
    }

    public long addMessage(Message message) throws TransactionErrorException {
        return (Long) saveObject(message);
    }

    public void deleteMessage(Message message, boolean forever) throws TransactionErrorException {
        if (forever) {
            deleteObject(message);
        } else if (getMessageById(message.getIdMessage()) != null) {
            message.setStatus(statusService.findByStatus(MessageStatusValue.deleted));
            saveObject(message);
        }
    }

    public void deleteMessage(Message message) throws TransactionErrorException {
        deleteMessage(message, false);
    }

    public void updateMessage(Message message) throws NoSuchRecordException, TransactionErrorException {
        Message old = getMessageById(message.getIdMessage());
        if (old == null) {
            throw new NoSuchRecordException("The message with " + message.getIdMessage() + " id doesn't exist");
        }
        if (!old.getText().equals(message.getText())) {
            message.setStatus(statusService.findByStatus(MessageStatusValue.edited));
        }
        saveObject(message);
    }
}
