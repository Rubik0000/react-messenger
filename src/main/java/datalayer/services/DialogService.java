package datalayer.services;

import datalayer.entities.Dialog;
import datalayer.services.exceptions.NoSuchRecordException;
import datalayer.services.exceptions.TransactionErrorException;

/**
 * The class to provide operations in the DB above dialogs
 */
public class DialogService extends AbstractService {
    /**
     * Finds a dialog by id
     *
     * @param id the dialog id
     * @return the found dialog or null if a dialog was not found
     */
    public Dialog getDialogById(long id) {
        return getById(Dialog.class, "idDialog", id);
    }

    /**
     * Adds the new dialog to the DB
     *
     * @param dialog the dialog to add
     * @return the id of the new dialog
     * @throws TransactionErrorException if an error occurs due transaction
     */
    public long addDialog(Dialog dialog) throws TransactionErrorException {
        return (Long) saveObject(dialog);
    }

    /**
     * Deletes dialog
     *
     * @param dialog the dialog to delete
     * @param forever if true the dialog will be deleted from the DB else it will be marked as deleted
     * @throws TransactionErrorException if an error occurs due transaction
     */
    public void deleteDialog(Dialog dialog, boolean forever) throws TransactionErrorException {
        if (forever) {
            deleteObject(dialog);
        } else if (getDialogById(dialog.getIdDialog()) != null) {
            dialog.setDeleted(true);
            saveObject(dialog);
        }
    }

    /**
     * Marks dialog as deleted
     *
     * @param dialog the dialog to mark
     * @throws TransactionErrorException if an error occurs due transaction
     */
    public void deleteDialog(Dialog dialog) throws TransactionErrorException {
        deleteDialog(dialog, false);
    }

    /**
     * Updates a dialog
     *
     * @param dialog the dialog to update
     * @throws TransactionErrorException if an error occurs due transaction
     * @throws NoSuchRecordException if the dialog doesn't exist in the DB
     */
    public void updateDialog(Dialog dialog) throws TransactionErrorException, NoSuchRecordException {
        if (getDialogById(dialog.getIdDialog()) == null) {
            throw new NoSuchRecordException("The dialog with " + dialog.getIdDialog() + " id was not found");
        }
        saveObject(dialog);
    }
}
