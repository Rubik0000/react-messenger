package messenger;

import datalayer.services.UserService;
import datalayer.services.exceptions.RecordExistsException;
import datalayer.services.exceptions.TransactionErrorException;
import datalayer.sessions.SessionManager;
import datalayer.entities.User;
import org.hibernate.Session;
import org.hibernate.Transaction;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(value = "/t")
public class HelloServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        /*Session session = SessionManager.getInstance().openSession();
        Transaction tx = null;
        try {
            resp.getOutputStream().println("start transaction");
            tx = session.beginTransaction();
            resp.getOutputStream().println("after start transaction");
            User user = new User();
            user.setNickName("test");
            user.setPassword("0000");
            resp.getOutputStream().println("before save");
            session.save(user);
            resp.getOutputStream().println("save");
            tx.commit();
            resp.getOutputStream().println("it is ok");


        }
        catch (Exception e) {
            if (tx!=null) tx.rollback();
            e.printStackTrace();
            resp.getOutputStream().println("all is bad " + e.getMessage());
        } finally {
            session.close();
            resp.getOutputStream().println("close");
        }*/

        try {

            UserService userService = new UserService();
            User user1 = new User("test2", "test2");
            User user2 = new User("test3", "test3");
            userService.addUser(user1);
            userService.addUser(user2);
            resp.getOutputStream().println("ok");
        } catch (TransactionErrorException e) {
            resp.getOutputStream().println(e.getMessage());
        } catch (RecordExistsException e) {
            e.printStackTrace();
        }
        /*User user = userService.getUserById(1);
        if (user == null) {
            resp.getOutputStream().println("null");
        } else {
            resp.getOutputStream().println(user.getNickName() + " " + user.getPassword());
        }*/
        //RequestDispatcher dispatcher = this.getServletContext().getRequestDispatcher("/index.jsp");
        //dispatcher.forward(req, resp);
    }
}
