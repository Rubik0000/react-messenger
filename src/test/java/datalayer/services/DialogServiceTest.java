package datalayer.services;

import datalayer.entities.Dialog;
import datalayer.services.exceptions.NoSuchRecordException;
import datalayer.services.exceptions.TransactionErrorException;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

public class DialogServiceTest {
    static private DialogService dialogService;
    static private Dialog testDialog;

    @BeforeClass
    public static void initData() throws TransactionErrorException {
        dialogService = new DialogService();
        testDialog = new Dialog("test");
        long id = dialogService.addDialog(testDialog);
        testDialog.setIdDialog(id);
    }

    @AfterClass
    public static void clearData() throws TransactionErrorException {
        dialogService.deleteDialog(testDialog, true);
    }

    @Test
    public void dialogShouldBeFoundById() {
        Dialog dialog = dialogService.getDialogById(testDialog.getIdDialog());
        Assert.assertEquals(dialog.getIdDialog(), testDialog.getIdDialog());
    }

    @Test
    public void updatingDialogShouldWorksCorrectly() throws NoSuchRecordException, TransactionErrorException {
        String newName = "test123";
        Assert.assertNotEquals(newName, testDialog.getDialogName());
        testDialog.setDialogName(newName);
        dialogService.updateDialog(testDialog);
        Assert.assertEquals(newName, testDialog.getDialogName());
    }

    @Test(expected = NoSuchRecordException.class)
    public void updatingNotExistedDialogShouldThrowsException() throws NoSuchRecordException, TransactionErrorException {
        Dialog dialog = new Dialog("test2");
        dialogService.updateDialog(dialog);
    }

    @Test
    public void deletingDialogShouldMarkIt() throws TransactionErrorException {
        Assert.assertFalse(testDialog.isDeleted());
        dialogService.deleteDialog(testDialog);
        Dialog deleted = dialogService.getDialogById(testDialog.getIdDialog());
        Assert.assertTrue(deleted.isDeleted());
    }
}
