package datalayer.services;

import datalayer.entities.User;
import datalayer.services.exceptions.NoSuchRecordException;
import datalayer.services.exceptions.RecordExistsException;
import datalayer.services.exceptions.TransactionErrorException;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

public class UserServiceTest {
    static private UserService userService;
    static private User testUser;

    @BeforeClass
    static public void initData() throws RecordExistsException, TransactionErrorException {
        userService = new UserService();
        testUser = new User("test", "test");
        long id = userService.addUser(testUser);
        testUser.setIdUser(id);
    }

    @AfterClass
    static public void clearData() throws TransactionErrorException {
        userService.deleteUser(testUser);
    }

    @Test
    public void userShouldBeFoundById() {
        User user = userService.getUserById(testUser.getIdUser());
        Assert.assertEquals(user.getIdUser(), testUser.getIdUser());
    }

    @Test
    public void userShouldBeFoundByNickname() {
        User user = userService.getUserByNickname(testUser.getNickName());
        Assert.assertEquals(user.getIdUser(), testUser.getIdUser());
    }

    @Test(expected = RecordExistsException.class)
    public void addingExistedUserShouldThrowException() throws RecordExistsException, TransactionErrorException {
        userService.addUser(testUser);
    }

    @Test(expected = NoSuchRecordException.class)
    public void updateNotExistedUserShouldThrowsException() throws TransactionErrorException, NoSuchRecordException {
        User user = new User("test2", "test2");
        userService.updateUser(user);
    }

}
