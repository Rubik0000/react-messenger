package datalayer.services;

import datalayer.entities.MessageStatusValue;
import org.junit.Assert;
import org.junit.Test;

public class MessageStatusServiceTest {

    @Test
    public void messageStatusesShouldBeAddedToDb() {
        MessageStatusService messageStatusService = new MessageStatusService();
        Assert.assertEquals(messageStatusService.getAll().size(), MessageStatusValue.values().length);
    }
}
